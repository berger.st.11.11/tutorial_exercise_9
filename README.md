# Tutorial Exercise 9 - Gruppe 2
Gidon Bauer, Stefan Basermann, Ghina Aldardari, Stefan Berger

Erweiterung von NONLINEAR_SYSTEM zur Anpassung der freien Parameter p -> FITTING_SYSTEM/

# Übersetzen

Im obersten Ordner 'make' aufrufen übersetzt die Applikationen, zur Nutzung müssen nur die Header eingefügt werden.
Zur Nutzung von dco/c++ muss eine gültige Lizenz vorhanden sein, diese findet man unter extern_libraries/dco/nag_key.txt.
Dieser Pfad muss angegeben werden, dazu wird der erste Befehl beim Aufruf von 'make' verwendet.

# Aufräumen

Mit 'make clean' werden *.exe Dateien und die Dokumentation gelöscht.

# Dokumentation

Mit 'make doc' wird die Dokumentation mittels Doxygen erstellt.
Dazu muss doxygen und graphviz/dot installiert sein.
