all:
	export NAG_KUSARI_FILE=$(CURDIR)/extern_libraries/dco/nag_key.txt
	cd FITTING_SYSTEM && make


doc:
	cd FITTING_SYSTEM && make doc

clean:
	cd FITTING_SYSTEM && make clean

.PHONY: all doc clean

