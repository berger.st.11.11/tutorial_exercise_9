#pragma once

#include "parameter_fitting_system.hpp"


namespace Fitting {

/// This class is the parent for parameter fitting implementaion using the strategy pattern 

template<typename TS, typename TP, int NS, int NP>
class Fit_parameter{
    public:
    /// This is a virtual method which has to be implemented in the child classes
    virtual void fit_p(Parameter_fitting_system<TS,TP,NS,NP>&) =0;
};

}