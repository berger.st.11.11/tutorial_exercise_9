#pragma once

#include "dco.hpp"
#include "fit_parameter.hpp"
#include "nonlinear_solver.hpp"
#include "parameter_fitting_system.hpp"

namespace Fitting
{
  /// this class is a implementation of fit_parameter, it uses a gradient desecent method to minimize the residuum, until the norm of the derivitive of the residuum with respect to parameters is 0
  template <typename TS, typename TP, int NS, int NP>
  class Gradient_descent : public Fit_parameter<TS, TP, NS, NP>
  {
    Nonlinear::Solver<TS, TP, NS, NP> &_Nlsol;
    TS _eps;

  public:
    Gradient_descent(Nonlinear::Solver<TS, TP, NS, NP> &, const TS &);

    /// this method uses gradient descent to fit the parameter so that x equals x_target
    void fit_p(Parameter_fitting_system<TS, TP, NS, NP> &);

    /// this method calculates the gradient of the residuum with respect to parameters
    typename Nonlinear::System<TS, TP, NS, NP>::VTP
    drdp(Parameter_fitting_system<TS, TP, NS, NP> &);

    /// this method calculates the residuum of the parameter_fitting_system and writes it into the second argument
    void calc_residuum(Parameter_fitting_system<TS, TP, NS, NP> &, TS &);
  };

} // namespace Fitting

#include "../src/gradient_descent.cpp"
