#pragma once

#include <Eigen/Dense>
#include <nonlinear_system.hpp>

namespace Fitting
{

  /// this class is a extension of Nonlinear::System, it can be used to fit the final state x of the system to a target_x by adjusting the parameters using the fit_parameter class
  template <typename TS, typename TP, int NS, int NP>
  class Parameter_fitting_system : public Nonlinear::System<TS, TP, NS, NP>
  {
  public:
    using VTS = Eigen::Matrix<TS, NS, 1>;
    using MTS = Eigen::Matrix<TS, NS, NS>;

  protected:
    VTS _target_x;

  public:
  /// this constructor creates a parameter fitting system with the size specified in the parameters
    Parameter_fitting_system(int const, int const);
      /// this method returns a refrence to the vector of target_x 
    VTS &target_x();
  };

} // namespace Fitting

#include "../src/parameter_fitting_system.cpp"
