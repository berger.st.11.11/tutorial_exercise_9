namespace Fitting
{

  template <typename TS, typename TP, int NS, int NP>
  Parameter_fitting_system<TS, TP, NS, NP>::Parameter_fitting_system(int const ns, int const np) : Nonlinear::System<TS, TP, NS, NP>(ns, np), _target_x(ns) {}

  template <typename TS, typename TP, int NS, int NP>
  typename Parameter_fitting_system<TS, TP, NS, NP>::VTS &
  Parameter_fitting_system<TS, TP, NS, NP>::target_x()
  {
    return _target_x;
  }

} // namespace Fitting