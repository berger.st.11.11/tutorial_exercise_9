#include <dco.hpp>

namespace Fitting
{

  template <typename TS, typename TP, int NS, int NP>
  Gradient_descent<TS, TP, NS, NP>::Gradient_descent(
      Nonlinear::Solver<TS, TP, NS, NP> &Nlsol, const TS &eps)
      : _Nlsol(Nlsol), _eps(eps) {}

  template <typename TS, typename TP, int NS, int NP>
  void Gradient_descent<TS, TP, NS, NP>::fit_p(
      Parameter_fitting_system<TS, TP, NS, NP> &pfsys)
  {
    int iteration = 0;
    TS residuum;
    TS residuum_prev;
    auto last_p = pfsys.p();
    typename Nonlinear::System<TS, TP, NS, NP>::VTP grad = drdp(pfsys);

    calc_residuum(pfsys, residuum);

    while (grad.norm() > _eps)
    {
      calc_residuum(pfsys, residuum_prev);
      double alpha = 2;

      while (residuum_prev <= residuum && alpha >= _eps)
      {
        alpha /= 2;

        auto p_trial = last_p;
        p_trial -= alpha * grad;

        pfsys.p() = p_trial;

        calc_residuum(pfsys, residuum);
        iteration++;
      }
      pfsys.p() = last_p;
      calc_residuum(pfsys, residuum); //Wichtig, sonst berechnet Nlsol nicht die Ableitung von x
      pfsys.p() = last_p - alpha * grad;
      last_p = pfsys.p();

      grad = drdp(pfsys);
    }
  }

  template <typename TS, typename TP, int NS, int NP>
  void Gradient_descent<TS, TP, NS, NP>::calc_residuum(
      Parameter_fitting_system<TS, TP, NS, NP> &pfsys, TS &residuum)
  {
    _Nlsol.solve(pfsys);
    residuum = pow(((pfsys.x() - pfsys.target_x()).norm()), 2);
  }

  ///residuum nach p ableiten
  template <typename TS, typename TP, int NS, int NP>
  typename Nonlinear::System<TS, TP, NS, NP>::VTP
  Gradient_descent<TS, TP, NS, NP>::drdp(Parameter_fitting_system<TS, TP, NS, NP> &pfsys)
  {
    typename Nonlinear::System<TS, TP, NS, NP>::VTP grad(pfsys.np());
    typename Nonlinear::System<TS, TP, NS, NP>::VTS last;
    TS residuum;
    for (int i = 0; i < pfsys.np(); i++)
    {
      last = pfsys.x();
      dco::derivative(pfsys.p()(i)) = 1;
      calc_residuum(pfsys, residuum);
      grad(i) = dco::derivative(residuum);
      dco::derivative(pfsys.p()(i)) = 0;
      pfsys.x() = last;
    }
    calc_residuum(pfsys, residuum);
    return grad;
  }

} // namespace Fitting
