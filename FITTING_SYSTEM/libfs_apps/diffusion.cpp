#include <iostream>
#include <cassert>

#include "Fitting_diffusion.hpp"
#include "linear_solver_qr.hpp"
#include "nonlinear_solver_newton.hpp"
#include "gradient_descent.hpp"
#include "dco.hpp"

int main(int argc, char *argv[])
{
    assert(argc == 2);
    int s = std::stoi(argv[1]);
    assert(s > 1);

    using T = double;
    using DCO_T = dco::gt1s<T>::type;
    Fitting_Diffusion::System<DCO_T, DCO_T> pfsys(s);

    Linear::Solver_QR<DCO_T, Fitting_Diffusion::NS> lsol;
    Nonlinear::Solver_Newton<DCO_T, DCO_T, Fitting_Diffusion::NS, Fitting_Diffusion::NP> nlsol(lsol, 1e-7);
    Fitting::Gradient_descent<DCO_T, DCO_T, Fitting_Diffusion::NS, Fitting_Diffusion::NP> pfsol(nlsol, 1e-7);

    pfsys.p() = Nonlinear::System<DCO_T, DCO_T, Fitting_Diffusion::NS, Fitting_Diffusion::NP>::VTP::Random(pfsys.np());
    pfsys.x() = Nonlinear::System<DCO_T, DCO_T, Fitting_Diffusion::NS, Fitting_Diffusion::NP>::VTS::Random(pfsys.ns());

    nlsol.solve(pfsys);

    pfsys.target_x() = pfsys.x();

    pfsys.x() += Nonlinear::System<DCO_T, DCO_T, Fitting_Diffusion::NS, Fitting_Diffusion::NP>::VTS::Random(pfsys.ns());
    pfsys.p() += Nonlinear::System<DCO_T, DCO_T, Fitting_Diffusion::NS, Fitting_Diffusion::NP>::VTP::Random(pfsys.np());

    std::cout << std::endl;
    std::cout << "x_start=" << std::endl
              << pfsys.x() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "x_target=" << std::endl
              << pfsys.target_x() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "p_start=" << std::endl
              << pfsys.p() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    pfsol.fit_p(pfsys);
    std::cout << "p_solution=" << std::endl
              << pfsys.p() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "||x-target_x||^2 = " << pow((pfsys.x() - pfsys.target_x()).norm(), 2) << std::endl << std::endl;
}
