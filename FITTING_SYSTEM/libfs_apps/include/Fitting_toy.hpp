#pragma once

#include "parameter_fitting_system.hpp"
#include "utils.hpp"

namespace Parameter_fitting_Toy
{

  const int NS = Utils::Dynamic;
  const int NP = Utils::Dynamic;

  template <typename TS, typename TP>
  class System : public Fitting::Parameter_fitting_system<TS, TP, NS, NP>
  {
    using Nonlinear::System<TS, TP, NS, NP>::_x;
    using Nonlinear::System<TS, TP, NS, NP>::_p;

  public:
    ///This constructor creates the System with a dynamic state and parameter size
    System(int);
    /// this function returns the a vector with the evaluated functions
    typename Nonlinear::System<TS, TP, NS, NP>::VTS f();
    /// this function return a jacobi matrix of f with respect to the states x
    typename Nonlinear::System<TS, TP, NS, NP>::MTS dfdx();
  };
} // namespace Parameter_fitting_Toy

#include "../src/Fitting_toy.cpp"