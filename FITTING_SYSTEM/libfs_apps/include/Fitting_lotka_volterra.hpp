#pragma once

#include "parameter_fitting_system.hpp"

namespace Fitting_Lotka_Volterra
{

  const int NS = 2;
  const int NP = 4;

  ///This is an example to test the fit parameter class
  template <typename TS, typename TP>
  class System : public Fitting::Parameter_fitting_system<TS, TP, NS, NP>
  {
    using Nonlinear::System<TS, TP, NS, NP>::_x;
    using Nonlinear::System<TS, TP, NS, NP>::_p;

  public:
    ///This constructor creates the System with a static size
    System();
    /// this function returns the a vector with the evaluated functions
    typename Nonlinear::System<TS, TP, NS, NP>::VTS f();
    /// this function return a jacobi matrix of f with respect to the states x
    typename Nonlinear::System<TS, TP, NS, NP>::MTS dfdx();
  };

} // namespace Fitting_Lotka_Volterra
#include "../src/Fitting_lotka_volterra.cpp"