#pragma once

#include "parameter_fitting_system.hpp"
#include "utils.hpp"
#include "dco.hpp"

namespace Fitting_Diffusion
{

    const int NS = Utils::Dynamic;
    const int NP = 3;

    ///This is an example to test the fit parameter class
    template <typename TS, typename TP>
    class System : public Fitting::Parameter_fitting_system<TS, TP, NS, NP>
    {
        using Nonlinear::System<TS, TP, NS, NP>::_x;
        using Nonlinear::System<TS, TP, NS, NP>::_p;

    public:
        ///This constructor creates the System with a dynamic state size
        System(int);
        /// this function returns the a vector with the evaluated functions
        typename Nonlinear::System<TS, TP, NS, NP>::VTS f();
        /// this function return a jacobi matrix of f with respect to the states x
        typename Nonlinear::System<TS, TP, NS, NP>::MTS dfdx();
    };

} // namespace Fitting_Diffusion

#include "../src/Fitting_diffusion.cpp"
