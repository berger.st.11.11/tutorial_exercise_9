#include <iostream>
#include <random>

#include "dco.hpp"

#include "Fitting_toy.hpp"
#include "linear_solver_qr.hpp"
#include "nonlinear_solver_newton.hpp"

#include "gradient_descent.hpp"

int main(int argc, char* argv[]) {
  assert(argc == 2);
  const int n = std::stoi(argv[1]);
  assert(n > 0);

  using T = double;
  using DCO_T = dco::gt1s<T>::type;
  Parameter_fitting_Toy::System<DCO_T, DCO_T> pfsys(n);

  Linear::Solver_QR<DCO_T, Parameter_fitting_Toy::NS> lsol;
  Nonlinear::Solver_Newton<DCO_T, DCO_T, Parameter_fitting_Toy::NS, Parameter_fitting_Toy::NP> nlsol(lsol, 1e-7);
  Fitting::Gradient_descent<DCO_T, DCO_T, Parameter_fitting_Toy::NS, Parameter_fitting_Toy::NP> pfsol(nlsol, 1e-7);

  /// Setup fuer zufaellige Startwerte
  std::default_random_engine generator(std::random_device{}());
  std::uniform_real_distribution<T> dist_start_x (-10, 0);
  std::uniform_real_distribution<T> dist_start_p (-5, 5);
  std::uniform_real_distribution<T> dist_target_x (-10, 0);

  for (int i  = 0; i < n; ++i) {
    /// Start x
    pfsys.x()(i) = dist_start_x(generator);
    /// Start p
    pfsys.p()(i) = dist_start_p(generator);
    /// Target x
    pfsys.target_x()(i) = dist_target_x(generator);
  }

  std::cout << "x_start=\n" << pfsys.x() << std::endl << std::endl << std::endl;
  std::cout << "x_target=\n" << pfsys.target_x() << std::endl << std::endl << std::endl;
  std::cout << "p_start=\n" << pfsys.p() << std::endl << std::endl << std::endl;

  pfsol.fit_p(pfsys);

  std::cout << "p_solution=\n" << pfsys.p() << std::endl << std::endl << std::endl;
  std::cout << "||x-target_x||^2 = " << pow((pfsys.x() - pfsys.target_x()).norm(), 2) << std::endl << std::endl;

  return 0;
}
