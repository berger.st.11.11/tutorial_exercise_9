#include "dco.hpp"

namespace Parameter_fitting_Toy
{

  template <typename TS, typename TP>
  System<TS, TP>::System(int n) : Fitting::Parameter_fitting_system<TS, TP, NS, NP>(n, n) {}

  template <typename TS, typename TP>
  static inline typename Nonlinear::System<TS, TP, NS, NP>::VTS
  F(const typename Nonlinear::System<TS, TP, NS, NP>::VTS &x, const typename Nonlinear::System<TS, TP, NS, NP>::VTP &p)
  {
    typename Nonlinear::System<TS, TP, NS, NP>::VTS r(x.size());
    for (int i = 0; i < x.size(); ++i) r(i) = pow(p(i), 2) + x(i);
    return r;
  }

  template <typename TS, typename TP>
  typename Nonlinear::System<TS, TP, NS, NP>::VTS
  System<TS, TP>::f() { return F<TS, TP>(_x, _p); }

  template <typename TS, typename TP>
  typename Nonlinear::System<TS, TP, NS, NP>::MTS
  System<TS, TP>::dfdx()
  {

    typename Nonlinear::System<TS, TP, NS, NP>::MTS drdx(_x.size(), _x.size());
    using DCO_T = typename dco::gt1s<TS>::type;
    typename Nonlinear::System<DCO_T, TP, NS, NP>::VTS x_t(_x.size()), r_t(_x.size());
    for (auto i = 0; i < _x.size(); i++)
      dco::value(x_t(i)) = _x(i);
    for (auto i = 0; i < _x.size(); i++)
    {
      dco::derivative(x_t(i)) = 1;
      r_t = F<DCO_T, TP>(x_t, _p);
      for (auto j = 0; j < _x.size(); j++)
        drdx(j, i) = dco::derivative(r_t(j));
      dco::derivative(x_t(i)) = 0;
    }
    return drdx;
  }

} // namespace Parameter_fitting_Toy
