#include <iostream>

#include "Fitting_lotka_volterra.hpp"
#include "linear_solver_qr.hpp"
#include "nonlinear_solver_newton.hpp"
#include "gradient_descent.hpp"
#include "dco.hpp"

int main()
{

    using T = double;
    using DCO_T = dco::gt1s<T>::type;
    Fitting_Lotka_Volterra::System<DCO_T, DCO_T> pfsys;
    Linear::Solver_QR<DCO_T, Fitting_Lotka_Volterra::NS> lsol;
    Nonlinear::Solver_Newton<DCO_T, DCO_T, Fitting_Lotka_Volterra::NS, Fitting_Lotka_Volterra::NP> nlsol(lsol, 1e-7);
    Fitting::Gradient_descent<DCO_T, DCO_T, Fitting_Lotka_Volterra::NS, Fitting_Lotka_Volterra::NP> pfsol(nlsol, 1e-7);

    pfsys.p() = Nonlinear::System<DCO_T, DCO_T, Fitting_Lotka_Volterra::NS, Fitting_Lotka_Volterra::NP>::VTP::Random(pfsys.np());
    pfsys.x() = Nonlinear::System<DCO_T, DCO_T, Fitting_Lotka_Volterra::NS, Fitting_Lotka_Volterra::NP>::VTS::Random(pfsys.ns());

    nlsol.solve(pfsys);

    pfsys.target_x() = pfsys.x();

    pfsys.x() += Nonlinear::System<DCO_T, DCO_T, Fitting_Lotka_Volterra::NS, Fitting_Lotka_Volterra::NP>::VTS::Random(pfsys.ns());
    pfsys.p() += Nonlinear::System<DCO_T, DCO_T, Fitting_Lotka_Volterra::NS, Fitting_Lotka_Volterra::NP>::VTP::Random(pfsys.np());

    std::cout << std::endl;
    std::cout << "x_start=" << std::endl
              << pfsys.x() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "x_target=" << std::endl
              << pfsys.target_x() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "p_start=" << std::endl
              << pfsys.p() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    pfsol.fit_p(pfsys);
    std::cout << "p_solution=" << std::endl
              << pfsys.p() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "||x-target_x||^2 = " << pow((pfsys.x() - pfsys.target_x()).norm(), 2) << std::endl << std::endl;
}
